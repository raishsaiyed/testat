===================================
Environment
===================================
l_Environment=Dev
===================================
Server Install Path
===================================
i_InstallPathDev=\\d2ntapnas01\dv202c\Documentation\SupplyChain\LoadClose
===================================
Reference File name parameters
===================================
i_ReferenceFileNameDev=RPA.DevLoadClose.DC.Reference.xlsx
===================================
Notes
===================================
The Dev config file is DIFFERENT than the Prod config file.
For any changes to the BOT, DO NOT PACKAGE the config.txt file.

