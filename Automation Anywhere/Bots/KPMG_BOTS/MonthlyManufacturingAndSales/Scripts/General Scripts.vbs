Function getLastDateofLastMonth()
	stDate=DateAdd("d", -1, DateSerial(Year(Now), Month(Now), 1))
	getLastDateofLastMonth=Day(stDate) & "." & Month(stDate) & "." & Year(stDate)
	
End Function

Function getFirstDateOfLastMonth()
	stDate=DateSerial(Year(DateAdd("m", -1, Now)), Month(DateAdd("m", -1, Now)), 1)
	getFirstDateOfLastMonth=Day(stDate) & "." & Month(stDate) & "." & Year(stDate)
End Function	

Function getDateDiffInMonths(inDate)
	a=Split(inDate)
	fromDate=a(0)
	toDate=a(1)
	getDateDiffInMonths=DateDiff("m",fromDate,toDate)
End Function