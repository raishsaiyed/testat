If Not IsObject(application) Then
   Set SapGuiAuto  = GetObject("SAPGUI")
   Set application = SapGuiAuto.GetScriptingEngine
End If
If Not IsObject(connection) Then
   Set connection = application.Children(0)
End If
If Not IsObject(session) Then
   Set session    = connection.Children(0)
End If
If IsObject(WScript) Then
   WScript.ConnectObject session,     "on"
   WScript.ConnectObject application, "on"
End If

Dim valLayout
valLayout = WScript.Arguments.Item(0)
Set Layout = session.findById("wnd[1]/usr/subSUB_CONFIGURATION:SAPLSALV_CUL_LAYOUT_CHOOSE:0500/cntlD500_CONTAINER/shellcont/shell")

Rows = Layout.RowCount()

For i = 0 To Rows - 1

LayoutVariant = Layout.getCellValue(i, "VARIANT")

If LayoutVariant = valLayout Then

Layout.currentCellRow = i

Layout.clickCurrentCell
Layout.press

Exit For

End If

Next
